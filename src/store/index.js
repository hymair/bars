import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    layout: 'landscape',
    menu: false,
    widgetPanel: false
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
